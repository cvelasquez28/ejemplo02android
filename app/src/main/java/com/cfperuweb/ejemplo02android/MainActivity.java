package com.cfperuweb.ejemplo02android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText edtEdad;
    Button btnProcesar;
    TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //vinculando el componente
        edtEdad = findViewById(R.id.edtEdad);
        btnProcesar = findViewById(R.id.btnProcesar);
        tvResultado = findViewById(R.id.tvResultado);

        //cuando se pulse el boton
        btnProcesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int edad = Integer.parseInt(edtEdad.getText().toString());

                if(edad >= 18){ //Eres mayor de edad
                    tvResultado.setText("Usted es mayor de edad");
                }else{//Eres menor de edad
                    tvResultado.setText("Usted es menor de edad");
                }
                edtEdad.setText("");

            }
        });
    }
}